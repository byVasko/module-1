class PlayersController < ApplicationController
  def index
  	@players = Player.all
  end

  def new
  	@player = Player.new
  end

  def create
  	@player = Player.new(player_params)
  	if @player.save
  		redirect_to root_path
  	end
  end

  def show
  	@player = Player.find_by(id: params[:id])
  end

  def edit
  	@player = Player.find_by(id: params[:id])
  end

  def update
  	@player = Player.find_by(id: params[:id])
  	if @player.update
  		redirect_to  edit_player_path(:params)
  	end
  end

  def destroy
  	@player = Player.find_by(id: params[:id])
  	if @player.destroy
  		redirect_to player_path(:params)
  	end
  end
private
  def player_params
    params.require(:player).permit(:nickname, :rank, :charisma, :wisdom)
  end
end